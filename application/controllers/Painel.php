<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends CI_Controller {
		public function index()
		{	
			$this->load->view('common/config.php');
			$this->load->model('ProdutoModel');
			$this->ProdutoModel->login();
			$this->load->view('painel/login.php');
			$this->load->view('common/config-footer.php');
		}
		public function listar_produtos(){
			$this->load->view('common/config.php');
			$this->load->view('common/navbar-painel.php');
			$this->load->model('ProdutoModel');
			$data['resultados'] = $this->ProdutoModel->lista_produtos();
			$v['tabela'] = $this->load->view('painel/table_produtos.php', $data, true);
			$this->load->view('painel/listar_produtos.php', $v);
			$this->load->view('common/config-footer.php');
		}
		public function criar_produto(){
			$this->load->view('common/config.php');
			$this->load->view('common/navbar-painel.php');
			$this->load->model('ProdutoModel');
			
			$this->ProdutoModel->novo_produto();
			$this->load->view('painel/form_produto.php');
			$this->load->view('common/config-footer.php');
			}
		public function delete_produto($imagem, $id){
			$this->load->model('ProdutoModel');
			if ($this->ProdutoModel->delete_produto($imagem, $id)) {
					redirect(base_url('Painel/listar_produtos'));
					
			} else {
					log_message('error', 'Erro ao deletar...');
			}
		}
		public function alterar_produto($id){
			$this->load->view('common/config.php');
			$this->load->view('common/navbar-painel.php');
			$this->load->model('ProdutoModel');
			
			$data = $this->ProdutoModel->get_detalhe($id);
			$this->ProdutoModel->editar_produto($id);
			$this->load->view('painel/form_altera_produto.php', $data);
			$this->load->view('common/config-footer.php');
		}

}
