<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pizzaria extends CI_Controller {
		public function index()
		{	
			$this->load->view('common/config.php');
			$this->load->view('common/navbar.php');
			$this->load->view('index.php');
			$this->load->view('common/footer.php');
			$this->load->view('common/config-footer.php');

		} 
    	public function produto($tipo) {
			if(($tipo == 'bebida') || ($tipo == 'pizza') || ($tipo == 'promocoes')){
				$this->load->view('common/config.php');
				$this->load->view('common/navbar.php');
				$this->load->model('ProdutoModel');
				$data['resultados'] = $this->ProdutoModel->get_data($tipo);
				$v['titulo1'] = $this->load->view('produto/figure_produto', $data, true);
				$v['title'] = $this->uri->segment(3);
				$this->load->view('produto/produto.php', $v);
				$this->load->view('common/footer.php'); 
				$this->load->view('common/config-footer.php');
			}else{
				redirect(base_url('Pizzaria/produto/pizza'));
			}
		}
		public function detalhe_produto($id){
			$this->load->view('common/config.php');
			$this->load->view('common/navbar.php');
			$this->load->model('ProdutoModel');
			$data = $this->ProdutoModel->get_detalhe($id);
			if($id == $data["id"]){
				$v['detalhe'] = $this->load->view('detalhe_produto/figure_detalhe_produto', $data, true);
				$this->load->view('detalhe_produto/detalhe_produto.php', $v);	
			}else{
				redirect(base_url('Pizzaria'));
			}
			$this->load->view('common/footer.php'); 
			$this->load->view('common/config-footer.php');
						
		}
		public function sobre(){
			$this->load->view('common/config.php');
			$this->load->view('common/navbar.php');
			$this->load->view('sobre/sobre.php');
			$this->load->view('common/footer.php'); 
			$this->load->view('common/config-footer.php');
		}
		public function contato(){
			$this->load->view('common/config.php');
			$this->load->view('common/navbar.php');
			$this->load->view('contato/contato.php');
			$this->load->view('common/footer.php'); 
			$this->load->view('common/config-footer.php');
		}

}
