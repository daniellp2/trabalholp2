<?php

defined('BASEPATH') OR exit('NO direct script access allowed');

class ProdutoModel extends CI_Model{

    public function get_data($tipo){
        $sql = "SELECT * FROM produtos WHERE tipo='$tipo'";
        $res = $this->db->query($sql);
        $data = $res->result();
        return $data;
    }
    public function lista_produtos(){
        $sql = "SELECT * FROM produtos";
        $res = $this->db->query($sql);
        $data = $res->result();
        return $data;       
    }
    public function get_detalhe($id){
        $sql = "SELECT * FROM produtos WHERE id='$id'";
        $res = $this->db->query($sql);
        $data = $res->result_array();
        return $data[0];
    }
    public function novo_produto(){
        $nome = $this->input->post('nome_produto');
        $descricao = $this->input->post('descricao_produto');
        $preco = $this->input->post('preco');
        $tipo = $this->input->post('tipo');

        if(sizeof($_POST) == 0) return;
		$config['upload_path'] = './assets/img';
        $config['allowed_types'] = 'gif|png|jpg|jpeg';
        $config['file_name'] = rand(1,10000);
		$config['max_width'] = '1000';
		$config['max_height'] = '1000';

		$this->load->library('upload', $config);

			if(!$this->upload->do_upload('imagem')){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
            
            $file_data = $this->upload->data();
            $data = array("imagem" => $file_data['file_name'],"nome_produto" => $nome, "preco" => $preco,
            "descricao_produto" => $descricao, "tipo" => $tipo);
            $this->db->insert('produtos', $data);
            echo '<script>alert("Produto Inserido com sucesso!")</script>';
            redirect('Painel/listar_produtos');
		}

    }
    public function editar_produto($id){
        if($_POST){
            $config['upload_path'] = './assets/img';
            $config['allowed_types'] = 'gif|png|jpg|jpeg';
            $config['file_name'] = rand(1,10000);
            $config['max_width'] = '1000';
            $config['max_height'] = '1000';
    
            $this->load->library('upload', $config);
            
            if(!$this->upload->do_upload('imagem_atual')){
                $this->db->set('nome_produto', $this->input->post('nome_produto'));    
                $this->db->set('descricao_produto', $this->input->post('descricao_produto'));   
                $this->db->set('preco', $this->input->post('preco'));
                $this->db->set('tipo', $this->input->post('tipo'));
                $this->db->set('imagem', $this->input->post('imagem'));
                $this->db->where('id', $id);
                $this->db->update('produtos');
                echo '<script>alert("Produto Alterado com sucesso!")</script>';
                redirect('Painel/listar_produtos');
            }else{
                $file_data = $this->upload->data();
                $this->db->set('nome_produto', $this->input->post('nome_produto'));    
                $this->db->set('descricao_produto', $this->input->post('descricao_produto'));   
                $this->db->set('preco', $this->input->post('preco'));
                $this->db->set('tipo', $this->input->post('tipo'));
                $this->db->set('imagem', $file_data['file_name']);
                $this->db->where('id', $id);
                $this->db->update('produtos');
                echo '<script>alert("Produto Alterado com sucesso!")</script>';
                redirect('Painel/listar_produtos');
            }

        }
    }
    public function delete_produto($imagem, $id) {
        $caminho = './assets/img/'.$imagem;
        unlink($caminho);
        $this->db->where('id', $id);
        return $this->db->delete('produtos');

    }
    public function login(){
        if($_POST){
            $email = $this->input->post('email');
            $senha = $this->input->post('senha');

            $sql = "SELECT * FROM usuario WHERE email = '$email' AND senha = '$senha'";
            $res = $this->db->query($sql);
            if($res->num_rows() > 0){
                redirect('Painel/listar_produtos');
            }else{
                echo '<script>alert("Usuário ou senha incorretos!")</script>';
            }
        }
    }
    
}
?>