<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <form class="text-center p-5" method="POST" enctype="multipart/form-data">
                <p class="h4 mb-4">Alterar produto</p>
                <input type="text" id="nome_produto" name="nome_produto" class="form-control mb-4" placeholder="Nome do produto" value="<?= $nome_produto ?>">
                <input type="text" id="descricao_produto" name="descricao_produto" class="form-control mb-4" placeholder="Descrição do produto" value="<?= $descricao_produto?>">
                <input type="text" id="preco" name="preco" class="form-control mb-4" placeholder="Preço do produto" value="<?= $preco ?>">
                <label>Upload de Imagem</label><input type="file" id="imagem_atual" name="imagem_atual" class="form-control mb-4">
                <input type="hidden" id="imagem" name="imagem" class="form-control mb-4" value="<?= $imagem ?>">
                <label>Tipo do produto</label>
                <select class="browser-default custom-select mb-4" name="tipo">
                    <option value="" disabled>Selecione (png,jpg,jpeg,gif) tamanho máximo(250x250)</option>
                    <option value="<?= $tipo ?>"><?= $tipo ?></option>
                    <option value="" disabled>------------</option>
                    <option value="pizza">Pizza</option>
                    <option value="bebida">Bebida</option>
                    <option value="promocoes">Promoções</option>
                </select>
                <button class="btn btn-dark btn-block" type="submit">Alterar</button>
            </form>
        </div>
    </div>
</div>
