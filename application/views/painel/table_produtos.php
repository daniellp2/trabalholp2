<div class="container">
<table class="table table-striped">
    <thead>
        <tr>
        <th scope="col">Id</th>
        <th scope="col">Nome</th>
        <th scope="col">Descrição</th>
        <th scope="col">Preço</th>
        <th scope="col"></th>
        <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($resultados as $resultado): ?>
        <tr>
        <th scope="row"><?= $resultado->id ?></th>
        <td><?= $resultado->nome_produto ?></td>
        <td><?= $resultado->descricao_produto?></td>
        <td>R$ <?= number_format($resultado->preco,2,",",".");?></td>
        <td><a href="<?= base_url('Painel/delete_produto/'.$resultado->imagem.'/'.$resultado->id) ?>"><i class="fas fa-trash"></i></a></td>
        <td><a href="<?= base_url('Painel/alterar_produto/'.$resultado->id)?>"><i class="fas fa-pencil-alt"></i></a></td>
        </tr>
        <tr>
<?php endforeach; ?>
</tbody>
    </table>
</div>
