<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <form class="text-center p-5" method="POST" enctype="multipart/form-data">
                <p class="h4 mb-4">Cadastro de produto</p>
                <input type="text" id="nome_produto" name="nome_produto" class="form-control mb-4" placeholder="Nome do produto" required>
                <input type="text" id="descricao_produto" name="descricao_produto" class="form-control mb-4" placeholder="Descrição do produto" required>
                <input type="text" id="preco" name="preco" class="form-control mb-4" placeholder="Preço do produto" required>
                <label>Upload de Imagem</label><input type="file" id="imagem" name="imagem" class="form-control mb-4" required>
                <label>Tipo do produto</label>
                <select class="browser-default custom-select mb-4" name="tipo" required>
                    <option value="" disabled selected>Selecione (png,jpg,jpeg,gif) tamanho máximo(1280x800)</option>
                    <option value="pizza">Pizza</option>
                    <option value="bebida">Bebida</option>
                    <option value="promocoes">Promoções</option>
                </select>
                <button class="btn btn-dark btn-block" type="submit">Cadastrar</button>
            </form>
        </div>
    </div>
</div>
