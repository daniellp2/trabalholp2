<div class="container">
  <h1 style="text-align:center;" class="mt-5">Detalhe do produto</h1>
  <div class="row justify-content-center mb-5"> 
    <div class="col-12 col-md-5 ">
    <div class="card mt-5">
      <div class="view overlay">
        <img class="card-img-top img-fluid" src="<?= base_url('assets/img/'.$imagem)?>"alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>
      <div class="card-body">
        <h4 class="card-title"><?= $nome_produto ?></h4>
        <p class="card-text"><?= $descricao_produto ?></p>
        <p class="card-text">R$ <?= number_format($preco,2,",",".");?></p>
        <a href="<?= base_url('Pizzaria/produto/'.$tipo)?>" class="btn btn-orange darken-3">Voltar</a>
        <a href="https://wa.me/5511970504966" class="btn btn-success">Comprar</a>
      </div>
    </div>
  </div>   
  </div>
</div>
