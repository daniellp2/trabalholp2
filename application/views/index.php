<div class="container">
  <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-2" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <div class="view">
          <img class="d-block w-100" src="./assets/img/banner.jpg" alt="First slide">
          <div class="mask rgba-black-light"></div>
        </div>
        <div class="carousel-caption">
          <p>Av. Salgado Filho, 253, Centro - Guarulhos - SP</p>
        </div>
      </div>
      <div class="carousel-item "> 
        <div class="view">
          <img class="d-block w-100" src="./assets/img/banner2.png" alt="Second slide">
          <div class="mask rgba-black-light"></div>
        </div>
        <div class="carousel-caption">
          <h4 class="h4-responsive">Disque Delivery</h4>
          <p><i class="fas fa-phone"></i> 2400-4966</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <h1 class="mt-5 gradient">Veja as fotos do nosso espaço:</h1>
  <div class="row mt-5">
    <div class="col-lg-4 col-md-4 mb-3">
      <img src="assets/img/pizzaria1.jpg" class="img-fluid"
        alt="Responsive image">
    </div>
    <div class="col-lg-4 col-md-4 mb-3">
      <img src="assets/img/pizzaria2.jpg" class="img-fluid"
        alt="Responsive image">
    </div>
    <div class="col-lg-4 col-md-6 mb-3">
      <img src="assets/img/pizzaria3.jpg" class="img-fluid"
        alt="Responsive image">
    </div>
  </div>
  <h1 class="mt-5 mb-5 gradient">O que os clientes acham da gente:</h1>
  <div class="jumbotron text-center hoverable p-4">
    <div class="row">
      <div class="col-md-3 offset-md-1 mx-3 my-3">
        <div class="view overlay">
          <img src="assets/img/mulher.png" class="img-fluid" alt="Sample image for first version of blog listing">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
      </div>
      <div class="col-md-7 text-md-left mt-3">
        <h4 class="h4 mb-4">Pizzaria nota 10!</h4>
        <p class="font-weight-normal">O atendimento é super educado, a pizza chega bem rápido e é muito bem feita, sou cliente
        a anos e não troco de pizzaria por nada!</p>
        <p class="font-weight-normal">por <a><strong>Carine Silva</strong></a>, 20/02/2019</p>
      </div>
    </div>
  </div>
</div>