<div class="row">
<?php foreach ($resultados as $resultado): ?>
    <div class="col-12 col-md-3">
        <div class="card mt-5 mb-1" id="tamanho">
            <div class="view overlay">
                <img class="card-img-top img-fluid" src="<?= base_url('assets/img/'.$resultado->imagem)?>" alt="Card image cap">
                <a href="#!">
                <div class="mask rgba-white-slight"></div>
                </a>
            </div>
            <div class="card-body">
                <h4 class="card-title"><?= $resultado->nome_produto ?></h4>
                <p class="card-text">R$ <?= number_format($resultado->preco,2,",",".");?></p>
                        <div class="d-flex justify-content-around">
                    <a href="<?= base_url('Pizzaria/detalhe_produto/'.$resultado->id)?>" class="btn btn-primary" title="Detalhes"><i class="fas fa-info"></i></a>
                    <a href="https://wa.me/5511970504966" target="_blank" class="btn btn-success" title="Comprar - WhatsApp"><i class="fas fa-shopping-cart"></i></a>
                </div>
            </div>
        </div>
    </div>   
<?php endforeach; ?>
</div>
