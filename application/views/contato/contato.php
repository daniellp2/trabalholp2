
<h3 class="display-4 gradient">Contato</h3>
<div class="jumbotron jumbotron-fluid">
  <div class="container">
  <h3 class="display-5 gradient">Dados para contato</h3>
    <p class="lead mt-5">Nosso restaurante fica na Av. Salgado Filho, 253, Centro - Guarulhos - SP</p>
    <p class="lead">Disque Delivery: 2400-4964</p>
    <p class="lead">WhatsApp: (11) 97050-4966</p>
    <h3 class="display-5 gradient mt-5">Siga a gente nas redes sociais</h3>
    <div class="d-flex justify-content-center mt-5">
        <a href="https://www.facebook.com/ifspguarulhos/" target="_blank"><img class="mr-2" src="<?= base_url('assets/img/face.png')?>" height="67"></a>
        <a href="https://www.instagram.com/ifsp_sjc/?hl=pt-br" target="_blank"><img class="mr-2" src="<?= base_url('assets/img/insta.png')?>" height="65"></a>
        <a href="https://twitter.com/ifspguarulhos" target="_blank"><img src="<?= base_url('assets/img/tt.png')?>" height="65"></a>
    </div>
  </div>
</div>
