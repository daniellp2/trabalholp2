
<footer class="page-footer font-small peach-gradient pt-4 mt-4">

    
    <div class="container-fluid text-center text-md-left">

      
      <div class="row justify-content-around">

        
        <div class="col-md-3 mt-md-0 mt-3">

          
          
            <h5 class="text-uppercase">PIZZARIA IFSP</h5>
            <p class="">Sua satisfação completa ou seu<br> dinheiro de volta!</p>
        </div>
        

        <hr class="clearfix w-100 d-md-none pb-3">

        
        <div class="col-md-3 mb-md-0 mb-3">

            
            <h5 class="text-uppercase">Links</h5>

            <ul class="list-unstyled">
              <li>
                <a href="<?= base_url('Pizzaria')?>">HOME</a>
              </li>
              <li>
                <a href="<?= base_url('Pizzaria/sobre') ?>">SOBRE</a>
              </li>
              <li>
                <a href="<?= base_url('Pizzaria/contato')?>">CONTATO</a>
              </li>
              
            </ul>

          </div>

      </div>


    </div>



    <div class="footer-copyright text-center py-3">© 2019 Copyright:
      <a href=""> IFSP Guarulhos</a>
    </div>


  </footer>
