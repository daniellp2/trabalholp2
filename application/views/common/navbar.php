<body>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark peach-gradient">
    <a class="navbar-brand pr-4" href="<?= base_url('Pizzaria') ?>"><img src="<?= base_url('assets/img/if.png')?>" height="40">
    <span class="gradient">SP Pizzaria</span>  </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
        aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="<?= base_url('Pizzaria') ?>">Home
            <span class="sr-only">(current)</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">Cardápio
            </a>
            <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
            <a class="dropdown-item" href="<?= base_url('Pizzaria/produto/pizza') ?>">Pizzas</a>
            <a class="dropdown-item" href="<?= base_url('Pizzaria/produto/bebida') ?>">Bebidas</a>
            <a class="dropdown-item" href="<?= base_url('Pizzaria/produto/promocoes') ?>">Promoções</a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('Pizzaria/sobre')?>">Sobre</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('Pizzaria/contato')?>">Contato</a>
        </li>
        </ul>
        <ul class="navbar-nav ml-auto nav-flex-icons">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-default" id="float-pequeno" aria-labelledby="navbarDropdownMenuLink-333">
            <a class="dropdown-item" href="<?= base_url('Painel')?>">Painel Administrativo</a>
            </div>
        </li>
        </ul>
    </div>
    </nav>
    