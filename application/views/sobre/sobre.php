<div class="container mt-5">
    <div class="row">
        <div class="jumbotron text-center">

        <h4 class="card-title h4 pb-2 gradient"><strong>Nossa Missão!</strong></h4>

        <div class="view overlay my-4">
            <img src="<?= base_url('assets/img/pizza.jpg') ?>" class="img-fluid" alt="">
            <a href="">
            <div class="mask rgba-white-slight"></div>
            </a>
        </div>

        <h5 class="indigo-text h5 mb-4">IFSP Pizzaria</h5>

        <p class="card-text text-justify">A IFSP Pizzaria foi fundada em 2010 e de lá para cá ganhou muitos clientes, nossa
        missão é trazer um produto de qualidade, com um custo justo e a entrega rápida. Entramos nas casas
        das pessoas e conquistamos espaço, pela qualidade de nosso serviço e nossa paixão pelo negócio,
        somos um time apaixonado por pizza, e agradecidos pela preferência de nossos clientes, venha você também
        junte-se a família IFSP Pizzaria!</p>
        </div>
    </div>
    <h1 class="mt-5 mb-5 gradient">Comentários dos nossos clientes</h1>
  <div class="jumbotron text-center hoverable p-4">
    <div class="row">
      <div class="col-md-3 offset-md-1 mx-3 my-3">
        <div class="view overlay">
          <img src="<?= base_url('assets/img/mulher.png')?>" class="img-fluid" alt="Sample image for first version of blog listing">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
      </div>
      <div class="col-md-7 text-md-left mt-3">
        <h4 class="h4 mb-4">Pizzaria nota 10!</h4>
        <p class="font-weight-normal">O atendimento é super educado, a pizza chega bem rápido e é muito bem feita, sou cliente
        a anos e não troco de pizzaria por nada!</p>
        <p class="font-weight-normal">por <a><strong>Carine Silva</strong></a>, 20/02/2019</p>
      </div>
    </div>
  </div>
  <div class="jumbotron text-center hoverable p-4">
    <div class="row">
      <div class="col-md-3 offset-md-1 mx-3 my-3">
        <div class="view overlay">
          <img src="<?= base_url('assets/img/homem.jpg')?>" class="img-fluid" alt="Sample image for first version of blog listing">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
      </div>
      <div class="col-md-7 text-md-left mt-3">
        <h4 class="h4 mb-4">A Pizza maisss deliciosa de Guarulhos!</h4>
        <p class="font-weight-normal">Melhor pizzaria da região, todo domingo é dia de pizza em casa, e sempre conto
        com a IFSP Pizzaria!</p>
        <p class="font-weight-normal">por <a><strong>Lucas Rodrigues</strong></a>, 19/01/2019</p>
      </div>
    </div>
  </div>
  <div class="jumbotron text-center hoverable p-4">
    <div class="row">
      <div class="col-md-3 offset-md-1 mx-3 my-3">
        <div class="view overlay">
          <img src="<?= base_url('assets/img/homem2.jpg')?>" class="img-fluid" alt="Sample image for first version of blog listing">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
      </div>
      <div class="col-md-7 text-md-left mt-3">
        <h4 class="h4 mb-4">IFSP Pizzaria é show!</h4>
        <p class="font-weight-normal">Pizza de qualidade, adoro pizza! hummm</p>
        <p class="font-weight-normal">por <a><strong>Rodrigo Sousa</strong></a>, 14/01/2019</p>
      </div>
    </div>
  </div>
</div>