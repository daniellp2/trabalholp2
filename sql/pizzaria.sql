﻿-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06-Mar-2019 às 20:38
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizzaria`
--
CREATE DATABASE pizzaria;
-- --------------------------------------------------------
use pizzaria;
--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `nome_produto` varchar(100) NOT NULL,
  `preco` float NOT NULL,
  `imagem` varchar(100) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descricao_produto` varchar(100) NOT NULL,
  `tipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome_produto`, `preco`, `imagem`, `last_modified`, `descricao_produto`, `tipo`) VALUES
(1, 'Coca-Cola (2L)', 5.5, '9947.jpg', '2019-03-05 00:43:07', 'Refrigerante Coca-Cola (2L)', 'bebida'),
(2, 'Itubaína (2L)', 4, '1281.jpg', '2019-03-05 00:44:18', 'Refrigerante Itubaína (2L)', 'bebida'),
(3, 'Dolly (2L)', 3, '8.jpg', '2019-03-05 00:54:17', 'Refrigerante Dolly (2L)', 'bebida'),
(4, 'Sprite (2L)', 4, '4586.jpg', '2019-03-05 01:03:03', 'Refrigerante Sprite (2L)', 'bebida'),
(6, 'Coca-Cola lata (350 ml)', 3, '8485.jpg', '2019-03-05 01:06:11', 'Refrigerante Coca-Cola lata (350 ml)', 'bebida'),
(7, 'Itubaína lata (350 ml)', 3, '644.jpg', '2019-03-05 01:08:43', 'Refrigerante Itubaína lata (350ml) - zero', 'bebida'),
(8, 'Sprite lata (350 ml)', 3, '2310.png', '2019-03-05 01:10:34', 'Refrigerante Sprite lata (350 ml)', 'bebida'),
(9, 'Pepsi lata (350 ml)', 4, '4515.png', '2019-03-05 01:12:10', 'Refrigerante Pepsi lata (350 ml)', 'bebida'),
(10, 'Pizza de Mussarela', 20, '1359.jpg', '2019-03-05 16:05:26', 'Ingredientes: Queijo Mussarela e tomate', 'pizza'),
(11, 'Pizza de Calabresa', 20, '6239.jpg', '2019-03-05 16:08:19', 'Ingredientes: Calabresa e Cebola', 'pizza'),
(12, 'Pizza de Frango com Catupiry', 25, '3193.jpg', '2019-03-05 16:09:53', 'Ingredientes: Frango, Azeitona e Catupiry', 'pizza'),
(13, 'Pizza Portuguesa Especial', 25, '6916.jpg', '2019-03-05 17:07:09', 'Ingredientes: Queijo, tomate e azeitona', 'pizza'),
(14, 'Combo 1', 55, '5692.jpg', '2019-03-05 17:38:56', '2 Pizzas Grande', 'promocoes'),
(15, 'Combo 2', 22.5, '3510.jpeg', '2019-03-05 17:38:44', 'Pizza mini + 1 refrigerante (1L)', 'promocoes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `email`, `senha`) VALUES
(1, 'master@pizzariaifsp.com.br', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
